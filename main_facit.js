//använd alert för att se att kopplingen till html-filen fungerar
console.log("works")

//funktion som uppdaterar tid en gång i sekunden
const setDate = () => {
    $("#time").html(new Date())
    setTimeout(setDate, 1000)
}

//kör funktionen
setDate()

// datastruktur för att hålla temperaturmätningar per timestamp
const data = [
    { date: "2018-01-01", temp: "1" },
    { date: "2018-01-02", temp: "2" },
    { date: "2018-01-03", temp: "3" },
    { date: "2018-01-04", temp: "4" },
    { date: "2018-01-05", temp: "5" },
]


// stoppa in respektivetemperaturmätning som en rad i tabellen
data.forEach(item => {
    $("#table-body").append(`
    <tr>
        <td>${item.date}</td>
        <td>${item.temp}</td>  
    </tr>
    `)
})





